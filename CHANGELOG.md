Current version is 0.4.

### Version 0.4
1. Code refactoring
2. Changes related to maintenance and debugging
3. Got rid of the idapythonrc.py file 

### Version 0.3
1. Added support for renaming stack variables.
2. Added support for global and local names.
3. Created a psida plugin.
4. Bug fixes.


### Version 0.2
1. Add persistent configuration via the idb_push.configure() function.


### Version 0.1
1. Add the basic functionality for online and offline work, supporting function names and comments.
